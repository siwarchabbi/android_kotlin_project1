package com.example.randomnumbergussing

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.util.Log
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import java.util.*
import kotlin.random.Random

class MainActivity2 : AppCompatActivity() {
    lateinit var textView: TextView
    lateinit var editText: EditText
    lateinit var imageButtonReset : ImageButton
    lateinit var imageButtonCheck : ImageButton


    var random: Int= Random.nextInt(1, 100)
    var guessHistory = mutableListOf<String>()
    @SuppressLint("SetTextI18n")
    lateinit var textToSpeech: TextToSpeech

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        textToSpeech = TextToSpeech(this) { status ->
            if (status == TextToSpeech.SUCCESS) {
                val result = textToSpeech.setLanguage(Locale.US)
                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                    Log.e("TTS", "Language is not supported or missing data")
                }
            } else {
                Log.e("TTS", "Initialization failed")
            }
        }
        textView = findViewById(R.id.textView)
        editText = findViewById(R.id.editText)
        imageButtonReset = findViewById(R.id.imageButtonReset)
        imageButtonCheck = findViewById(R.id.imageButton2Check)
        textView.text ="Please enter your guess :"
        updateHistoryTextView()

        imageButtonCheck.setOnClickListener {
            val number: Int = editText.text.toString().toInt()
            if (number < random){
            textView.text = "Wrong , your number is too low!"
            val text = "Wrong, your number is too low!"
            textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, null)
            editText.text.clear()
        } else if (number > random) {
            textView.text = "Wrong , your number is too high!"
            val text = "Wrong, your number is too high!"
            textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, null)
            editText.text.clear()
            } else {
                val message = "Congratulations, your number is right"
                textView.text = message
                editText.text.clear()

                val builder: AlertDialog.Builder = AlertDialog.Builder(this)
                builder.setTitle("Congratulations")
                builder.setMessage(message)
                builder.setPositiveButton("OK") { dialog, which ->
                    finish()
                }
                builder.setCancelable(false)
                builder.show()


                textToSpeech.speak(message, TextToSpeech.QUEUE_FLUSH, null, null)
            }

            guessHistory.add("$number")
            editText.text.clear()
            updateHistoryTextView()

        }
        imageButtonReset.setOnClickListener{
            rest()
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        textToSpeech.stop()
        textToSpeech.shutdown()
    }
    fun rest(){
        random = Random.nextInt(1, 100)
        textView.text ="Please enter your guess :5"
        guessHistory.clear()
        updateHistoryTextView()
        editText.text.clear()
    }
    private fun updateHistoryTextView() {
        val historyText = guessHistory.joinToString(", ")
        val historyTextView = findViewById<TextView>(R.id.guessHistoryTextView)
        historyTextView.text = "Guess History: $historyText"
    }
    }
