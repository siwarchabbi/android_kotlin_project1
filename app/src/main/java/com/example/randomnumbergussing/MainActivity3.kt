package com.example.randomnumbergussing

import android.annotation.SuppressLint
import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import android.speech.tts.TextToSpeech
import android.util.Log
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import java.util.Locale
import kotlin.random.Random

class MainActivity3 : AppCompatActivity() {
    lateinit var textView: TextView
    lateinit var editText: EditText
    lateinit var imageButtonReset : ImageButton
    lateinit var imageButtonCheck : ImageButton


    var random: Int= Random.nextInt(1, 100)
    var isGameFinished = false
    @SuppressLint("SetTextI18n")
    lateinit var textToSpeech: TextToSpeech

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main3)
        textToSpeech = TextToSpeech(this) { status ->
            if (status == TextToSpeech.SUCCESS) {
                val result = textToSpeech.setLanguage(Locale.US)
                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                    Log.e("TTS", "Language is not supported or missing data")
                }
            } else {
                Log.e("TTS", "Initialization failed")
            }
        }
        textView = findViewById(R.id.textView)
        editText = findViewById(R.id.editText)
        imageButtonReset = findViewById(R.id.imageButtonReset)
        imageButtonCheck = findViewById(R.id.imageButton2Check)
        textView.text ="Please enter your guess :"
        val timer = object : CountDownTimer(20000, 1000) {
            override fun onTick(millisUntilFinished: Long) {}

            override fun onFinish() {
                if (!isGameFinished) {
                    displayTimeUpAlert()
                }
            }
        }
        timer.start()
        imageButtonCheck.setOnClickListener {
            val number: Int = editText.text.toString().toInt()
            if (number < random){
                textView.text = "Wrong , your number is too low!"
                val text = "Wrong, your number is too low!"
                textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, null)
                editText.text.clear()
            } else if (number > random) {
                textView.text = "Wrong , your number is too high!"
                val text = "Wrong, your number is too high!"
                textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, null)
                editText.text.clear()
            } else {
                val message = "Congratulations, your number is right"
                textView.text = message
                editText.text.clear()
                isGameFinished = true

                val builder: AlertDialog.Builder = AlertDialog.Builder(this)
                builder.setTitle("Congratulations")
                builder.setMessage(message)
                builder.setPositiveButton("OK") { dialog, which ->
                    finish()
                }
                builder.setCancelable(false)
                builder.show()

                textToSpeech.speak(message, TextToSpeech.QUEUE_FLUSH, null, null)
            }


        }
        imageButtonReset.setOnClickListener{
            rest()
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        textToSpeech.stop()
        textToSpeech.shutdown()
    }

    fun rest(){
        random = Random.nextInt(1, 100)
        textView.text ="Please enter your guess :5"
        editText.text.clear()

    }
    fun displayTimeUpAlert() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle("Time's Up")
        builder.setMessage("Sorry, you've passed your time.")
        builder.setPositiveButton("OK") { dialog, which ->
            finish() // Close the activity
        }
        builder.setCancelable(false)
        builder.show()

        // Speak the message
        val text = "Time's Up. Sorry, you've passed your time."
        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, null)
    }

}